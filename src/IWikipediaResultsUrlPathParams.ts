// import { RouteProps } from "react-router";

export default interface IWikipediaResultsUrlPathParams /*extends RouteProps*/ {
    searchQuery?: string;
}