import React, { Component } from "react";
import { Link } from "react-router-dom";
import IGlobalState from "./IGlobalState";
import INavigationProps from "./INavigationProps";
import { connect } from "react-redux";

class Navigation extends Component<INavigationProps, {}> {
  render() {
    return (
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/app">React App Home</Link>
          </li>
          <li>
            <Link to="/todo">Todo App Home ({this.props.todoListSize})</Link>
          </li>
          <li>
            <Link to="/wiki">Wikipedia</Link>
          </li>
          <li>
            <Link to="/wiki/results">Wikipedia Results</Link>
          </li>
        </ul>
      </div>
    );
  }
}

let mapStateToProps = (globalState: IGlobalState): INavigationProps => {
  return {
    todoListSize: globalState.todosStore.todos.length
  }
}
let mapDispatchToProps = {
};

let NavigationContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Navigation);

export default NavigationContainer;
