export default interface IAction {
    type: string;
    payload?: Object;
    response?: Object;
}