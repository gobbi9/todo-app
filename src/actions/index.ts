import IAction from "./IAction";
import { Action, ActionCreator, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import IWikipediaResultsState from "../IWikipediaResultsState";

export function addTodo(description: string): IAction {
  return { type: "ADD_TODO", payload: description };
}

export function inputTodo(description: string): IAction {
  return { type: "INPUT_TEXT_TODO", payload: description };
}

export const wikipediaSearch: ActionCreator<
  ThunkAction<void, IWikipediaResultsState, void, Action>
> = (keyword: string) => {
  return (dispatch: Dispatch<IAction>): Promise<IAction> => {
    return fetch(
      `https://en.wikipedia.org/w/api.php?origin=*&action=query&list=search&srsearch=${keyword}&format=json`,
      {
        //method: "GET",
        headers: {
          Accept: "application/json"
        }
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        return dispatch({
          type: "WIKIPEDIA_SEARCH",
          payload: keyword,
          response: responseJson
        });
      });
  };
};

// export function wikipediaSearch(keyword: string): (_: any) => any {
//   return function(dispatch) {
//     let url = `https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=${keyword}&format=json&callback=JSONP_CALLBACK`;
//     superagent
//       .get(url)
//       .use(
//         jsonp({
//           callbackName: "JSONP_CALLBACK"
//         })
//       )
//       .end((err, res) => {
//         dispatch({ type: "WIKIPEDIA_SEARCH", keyword: keyword, res: res });
//       });
//   };
