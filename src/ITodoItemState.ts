export default interface ITodoItemState {
    id: number;
    description: string;
    done: boolean;
    highlighted: boolean;
}