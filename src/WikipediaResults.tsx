import React, { Component } from "react";
import IWikipediaResultsProps from "./IWikipediaResultsProps";
import IWikipediaResultsState from "./IWikipediaResultsState";
import { wikipediaSearch } from "./actions";
import { connect } from "react-redux";
import IGlobalState from "./IGlobalState";
import Panel from "./Panel";

class WikipediaResults extends Component<
  IWikipediaResultsProps,
  IWikipediaResultsState
> {
  constructor(props: IWikipediaResultsProps) {
    super(props);
  }

  componentDidMount() {
    let searchQuery = this.props.match.params.searchQuery;
    if (searchQuery) this.props.wikipediaSearch(searchQuery);
  }

  render() {
    return (
      <div>
        <h4>Wikipedia Results</h4>
        {this.props.response
          ? this.props.response.query.search.map(
              (searchResult: any, index: number) => {
                return (
                  <Panel key={index} title={searchResult.title}>
                    <span
                      dangerouslySetInnerHTML={{ __html: searchResult.snippet }}
                    />
                  </Panel>
                );
              }
            )
          : null}

        {/* <h4>Wikipedia Results ({this.props.match.params.searchQuery})</h4> */}
      </div>
    );
  }
}

// https://stackoverflow.com/questions/44118060/react-router-dom-with-typescript
let mapStateToProps = function(
  state: IGlobalState,
  ownProps: IWikipediaResultsProps
): IWikipediaResultsProps {
  return { ...ownProps, ...state.wikipediaResultsStore };
};

let mapDispatchToProps = {
  wikipediaSearch: wikipediaSearch
};

let WikipediaResultsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(WikipediaResults);

export default WikipediaResultsContainer;
