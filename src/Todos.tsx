import React, { Component } from "react";
import "./Todos.scss";
import ITodosProps from "./ITodosProps";
import ITodosState from "./ITodosState";
import TodoItem from "./TodoItem";
import { connect } from "react-redux";
import { addTodo as addTodoAction, inputTodo } from "./actions";
import IGlobalState from "./IGlobalState";
import Navigation from "./Navigation";

class Todos extends Component<ITodosProps, ITodosState> {
  constructor(props: ITodosProps) {
    super(props);
  }

  render() {
    // console.log(this.props);
    return (
      <div className="Todos-container">
        <div className="Todos-list">
          <Navigation />
          {this.props.loading || "Loaded"}
          <hr />
          {this.props.todos.map(todo => (
            <TodoItem key={todo.id} todoItem={todo} />
          ))}
          <div>
            <input
              className={this.props.inputTextStyle}
              type="text"
              placeholder="New todo"
              value={this.props.newTodoDescription}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                let inputFieldValue = event.currentTarget.value;
                this.props.inputTodo(inputFieldValue);
              }}
              onKeyPress={(event: React.KeyboardEvent<HTMLInputElement>) => {
                let inputFieldValue = event.currentTarget.value;
                if (event.key === "Enter" && inputFieldValue) {
                  this.props.addTodo(inputFieldValue);
                }
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = function(state: IGlobalState): ITodosProps {
  // console.log('map state to props ', state);
  return state.todosStore; // it must return a copy?
};

let mapDispatchToProps = {
  addTodo: addTodoAction,
  inputTodo: inputTodo
};

let TodosContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Todos);

export default TodosContainer;
