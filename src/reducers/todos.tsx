import ITodosState from "../ITodosState";
import ITodoItemState from "../ITodoItemState";
import IAction from "../actions/IAction";
import { deepClone } from "../utils";

// action = ADD_TODO, INIT, VALIDATE

const initialTodoState = {
  loading: false,
  todos: [
    { id: 1, description: "test", done: false, highlighted: false },
    { id: 2, description: "test2", done: false, highlighted: false },
    { id: 3, description: "test3", done: true, highlighted: false }
  ],
  newTodoDescription: "",
  addTodo: (_: string) => {
    return { type: "" };
  },
  inputTodo: (_: string) => {
    return { type: "" };
  },
  inputTextStyle: "Todos-pristine-input",
};

export default function todoReducer(
  state: ITodosState = initialTodoState,
  action: IAction
): ITodosState {
  if (action.type === "ADD_TODO") {
    let newTodo: ITodoItemState = {
      id: Math.max(...state.todos.map(todo => todo.id)) + 1,
      done: false,
      highlighted: false,
      description: action.payload as string
    };

    let newState: ITodosState = {
      ...deepClone(state),
      todos: [...deepClone(state.todos), newTodo],
      newTodoDescription: ""
    };

    return newState;
  } else if (action.type === "INPUT_TEXT_TODO") {
    let newTodoDescription = action.payload as string;
    let newState: ITodosState = {
      ...deepClone(state),
      newTodoDescription: newTodoDescription,
      inputTextStyle:
        !newTodoDescription || newTodoDescription.length === 0
          ? "Todos-invalid-input"
          : "Todos-valid-input"
    };

    return newState;
  }

  return state;
}
