import IAction from "../actions/IAction";
import { deepClone } from "../utils";
import IWikipediaResultsState from "../IWikipediaResultsState";
import IWikipediaResultsProps from "../IWikipediaResultsProps";
import { RouteComponentProps } from "react-router";
import IWikipediaResultsUrlPathParams from "../IWikipediaResultsUrlPathParams";

// action = ADD_TODO, INIT, VALIDATE

const initialWikipediaResultsState: IWikipediaResultsState = {
    wikipediaSearch: (_: string) => {
      return { type: "" };
    }
};

export default function wikipediaReducer(
  state: IWikipediaResultsState = initialWikipediaResultsState,
  action: IAction
): IWikipediaResultsState {
  if (action.type === "WIKIPEDIA_SEARCH") {
    return {...state, response: action.response};
  }

  return initialWikipediaResultsState;
}
