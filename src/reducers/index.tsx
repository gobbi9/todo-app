import { combineReducers } from "redux";
import todoReducer from "./todos";
import IGlobalState from "../IGlobalState";
import wikipediaReducer from "./wikipedia-results";

const rootReducer = combineReducers<IGlobalState>({
  todosStore: todoReducer,
  wikipediaResultsStore: wikipediaReducer
});

export default rootReducer;

export type AppState = ReturnType<typeof rootReducer>;
