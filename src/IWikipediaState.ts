export default interface IWikipediaState {
  searchString: string;
  buttonClicked: boolean;
}