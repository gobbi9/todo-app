import ITodoItemState from "./ITodoItemState";
import IAction from "./actions/IAction";

export default interface ITodosProps {
  loading: boolean;
  todos: Array<ITodoItemState>;
  newTodoDescription: string;
  addTodo(description: string): IAction;
  inputTodo(description: string): IAction;
  inputTextStyle: string;
}