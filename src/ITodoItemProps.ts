import ITodoItemState from "./ITodoItemState";

export default interface ITodoItemProps {
    todoItem: ITodoItemState;
}