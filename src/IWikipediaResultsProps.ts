import IWikipediaResultsUrlPathParams from "./IWikipediaResultsUrlPathParams";
import { RouteComponentProps } from "react-router";
import IAction from "./actions/IAction";
import IDontKnowWhatToExpect from "./IDontKnowWhatToExpect";

export default interface IWikipediaResultsProps /*extends RouteComponentProps<IWikipediaResultsUrlPathParams>*/ {
  response?: IDontKnowWhatToExpect;
  wikipediaSearch(keyword: string): IAction;
  [key: string]: any;
}
