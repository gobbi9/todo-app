import React, { Component } from "react";
import Navigation from "./Navigation";
import IWikipediaState from "./IWikipediaState";
import IWikipediaProps from "./IWikipediaProps";
import { Redirect } from "react-router";
class Wikipedia extends Component<IWikipediaProps, IWikipediaState> {
  constructor(props: IWikipediaProps) {
    super(props);

    this.state = {
      searchString: "",
      buttonClicked: false
    };

    this.search = this.search.bind(this);
    this.inputChanged = this.inputChanged.bind(this);
  }

  search() {
    this.setState({
      ...this.state,
      buttonClicked: true
    });
  }

  inputChanged(event: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      ...this.state,
      buttonClicked: false,
      searchString: event.target.value
    });
  }

  render() {
    return (
      <div>
        <Navigation />
        <h4>Wikipedia</h4>
        {this.state.buttonClicked ? (<Redirect to={`/wiki/results/${this.state.searchString}`}/>) : null}
        <input
          placeholder="Search criteria"
          value={this.state.searchString}
          onChange={this.inputChanged}
        />
        <button onClick={this.search}>Search</button>
      </div>
    );
  }
}

export default Wikipedia;
