import ITodosProps from "./ITodosProps";
import IWikipediaResultsProps from "./IWikipediaResultsProps";

export default interface IGlobalState {
    todosStore: ITodosProps;
    wikipediaResultsStore: IWikipediaResultsProps;
}