import React, { Component } from "react";
import { Link } from "react-router-dom";
import Navigation from "./Navigation";

class Home extends Component {
  render() {
    return (
      <div>
        <h1>Home</h1>
        <Navigation />
      </div>
    );
  }
}

export default Home;
