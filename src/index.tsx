import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import combineReducers from "./reducers";
import TodosContainer from "./Todos";
import { BrowserRouter, Route} from "react-router-dom";
import App from "./App";
import Home from "./Home";
import Wikipedia from "./Wikipedia";
import WikipediaResults from "./WikipediaResults";
import thunkMiddleWare, { ThunkMiddleware } from 'redux-thunk';
import IAction from "./actions/IAction";

let store = createStore(
  combineReducers,
  applyMiddleware(thunkMiddleWare as ThunkMiddleware<{}, IAction>)
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <div>
        <Route exact={true} path="/" component={Home} />
        <Route exact={true} path="/app" component={App} />
        <Route exact={true} path="/todo" component={TodosContainer} />
        <Route exact={true} path="/wiki" component={Wikipedia} />
        <Route exact={true} path="/wiki/results/:searchQuery?" component={WikipediaResults}  />
      </div>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
