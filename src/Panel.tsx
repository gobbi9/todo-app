import React, { Component } from 'react';
import "./Panel.css";
import IPanelProps from './IPanelProps';
import IPanelState from './IPanelState';

class Panel extends Component<IPanelProps, IPanelState> {
    render() {
        return (
            <div className="Panel">
                <h4 className="Panel-title">
                    {this.props.title}
                </h4>
                <div className="Panel-content">
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Panel;