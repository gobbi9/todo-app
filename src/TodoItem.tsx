import React, { Component } from "react";
import "./TodoItem.scss";
import ITodoItemProps from "./ITodoItemProps";
import ITodoItemState from "./ITodoItemState";

export default class TodoItem extends Component<
  ITodoItemProps,
  ITodoItemState
> {
  constructor(props: ITodoItemProps) {
    super(props);

    this.state = {
      ...props.todoItem,
      highlighted: false
    };

    this.checkTodo = this.checkTodo.bind(this);
    this.highlight = this.highlight.bind(this);
    this.resetHighlight = this.resetHighlight.bind(this);
  }

  checkTodo(): void {
    this.setState({
      ...this.state,
      done: !this.state.done
    });
  }

  highlight(): void {
    this.setState({
      ...this.state,
      highlighted: true
    });
  }

  resetHighlight(): void {
    this.setState({
      ...this.state,
      highlighted: false
    });
  }

  render() {
    return (
      <div
        onClick={this.checkTodo}
        onMouseEnter={this.highlight}
        onMouseLeave={this.resetHighlight}
        className={
          this.state.highlighted ? "TodoItem-highlighted TodoItem" : "TodoItem"
        }
      >
        {this.state.done ? "[x]" : "[ ]"} {this.state.description}
      </div>
    );
  }
}
